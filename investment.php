<?php
session_start();
require_once 'dao.php';

if(!$_SESSION['user']) {
    header("Location: login.php");
    die();
}
echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Investments</title>

    <!-- Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-light bg-light">
    <a class="navbar-brand" style="cursor: pointer">Crowdfunding</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
  
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/hwp/logout.php">Logout</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        ';
$investments = Dao::get_all_investments_by_project_id($_GET['projectId']);
if(sizeof($investments)==0) {
    header('Location: error.php?message=There is no such project or there is no investments for this project');
    die();
}
else {
    echo '<div class="col-sm-3">';
    if($investments[0]['projectOwnerId']!=$_SESSION['user']['id']) {
        header('Location: error.php?message=You are not owner of this project and thus cannot see its investors');
        die();
    }
    $totalInvestedSum = 0;
    foreach($investments as $i) {
        $totalInvestedSum+=$i['investmentFund'];
    }
    $remaining = $investments[0]['projectRequestedFund'] - $totalInvestedSum;
    echo "            <p>Total Raised : {{$totalInvestedSum}}</p>
            <p>Remaining : {{$remaining}}</p>";
    echo '
        </div>
        <div class="col-sm-9">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Participant</th>
                    <th scope="col">Contribution</th>
                </tr>
                </thead>
                <tbody>';
    foreach ($investments as $i) {
        echo "<tr>
                        <td>$i[userFirstName] $i[userLastName] ($i[userEmail])</td>
                        <td>$i[investmentFund]</td>
          </tr>";
    }
    echo '
                </tbody>
            </table>
        </div>
    </div>';
}
echo '
</div>
</body>
</html>';