<?php
session_start();
require_once 'dao.php';



if(!$_SESSION['user']) {
    header("Location: login.php");
    die();
}
echo '<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Home</title>

    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
    <nav class="navbar navbar-expand-md navbar-light bg-light">
    <a class="navbar-brand" style="cursor: pointer">Crowdfunding</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
  
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/hwp/logout.php">Logout</a>
            </li>
        </ul>
    </div>
</nav>
    <div class="container">
    <div class="row">
    <div class="col-sm-12">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Finishes</th>
            <th scope="col">Requested Fund</th>
            <th scope="col">Owner</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>';
$projects = Dao::get_all_projects();
while($p = $projects->fetch_assoc()) {
    echo "<tr>
                <td>".$p['id']."</td>
                <td>$p[name]</td>
                <td>$p[description]</td>
                <td>$p[endDate]</td>
                <td>$p[requestedFund]</td>
                <td>$p[userFirstName] $p[userLastName] ($p[userEmail])</td>
                <td><span>
                ";
    if($p['userId']!=$_SESSION['user']['id']) {
        echo "<a href='/hwp/createInvestment.php?projectId=$p[id]'><button class='btn btn-primary' type='button'>Invest</button></a>";
    } else {
        echo "<a href='/hwp/investment.php?projectId=$p[id]'><button class='btn btn-success' type='button'>View Investments</button></a>";
    }
    echo "</span></td>
          </tr>";
}
echo '
        </tbody>
    </table>
    </div>
    </div>
    </div>
    </body>
</html>
';

