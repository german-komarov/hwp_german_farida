<?php
session_start();
require_once 'dao.php';

if($_SERVER['REQUEST_METHOD']==='GET') {
    echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sign In</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-6">';
    if(isset($_GET['error'])) {
        echo '<div class="alert alert-danger" role="alert">
                    Bad credentials!
              </div>';
    }
    echo '
            <form action="/hwp/login.php" method="post">
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input name="email" type="email" class="form-control" placeholder="Enter email" id="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input name="password" type="password" class="form-control" placeholder="Enter password" id="pwd">
                </div>
                <button type="submit" class="btn btn-primary">Sign In</button>
            </form>
        </div>
        <div class="col-sm-2"></div>
    </div>

</div>
</body>
</html>';
} else {
    if(!isset($_POST['email']) || !isset($_POST['password'])) {
        header('Location: login.php?error=true');
        die();
    }
    $email = $_POST['email'];
    $password = $_POST['password'];
    $user = Dao::get_user_by_email($email);
    if($user==null || $user['password']!==$password) {
        header('Location: login.php?error=true');
        die();
    }

    unset($user['password']);
    $_SESSION['user'] = $user;
    header('Location: home.php');
    die();
}
