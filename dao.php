<?php

class Dao {
    const DBSERVERNAME = 'mysql-german.alwaysdata.net';
    const DBUSERNAME = "german";
    const DBPASSWORD = "It1Is2My3Password4";
    const DBNAME = "german_project_db";

    static function get_all_projects() {
        $conn = self::get_connection();
        $sql = "select p.idProject as id, p.projectName as name, p.projectDescription as description, 
        p.projectStartDate as startDate, p.projectEndDate as endDate, p.requestedFund, u.idUser as userId, 
        u.firstname as userFirstName, u.lastname as userLastName, u.email as userEmail 
        from projects p join users u on p.idUser = u.idUser";
        $result = $conn->query($sql);
        $conn->close();
        if($result->num_rows==0) {
            return null;
        }
        return $result;
    }

    static function get_user_by_email($email) {
        $conn = self::get_connection();
        $sql = "select idUser as id, firstname, lastname, email, password from users where email=?";
        $st = $conn->prepare($sql);
        $st->bind_param("s", $email);
        $st->execute();
        $result = $st->get_result();
        $conn->close();
        if($result->num_rows==0) {
            return null;
        }
        $user = null;
        while($row = $result->fetch_assoc()) {
            $user = $row;
            break;
        }
        return $user;
    }

    static function get_all_investments_by_project_id($projectId) {
        $conn = self::get_connection();
        $sql = "select investmentFund, investmentDate, p.requestedFund as projectRequestedFund, p.idUser as  projectOwnerId, 
        u.idUser as userId, u.firstname as userFirstName, u.lastname as userLastName, u.email as userEmail 
        from projects_investors pi join projects p on pi.idProject = p.idProject join users u on pi.idUser = u.idUser where p.idProject=?";
        $st = $conn->prepare($sql);
        $st->bind_param('i', $projectId);
        $st->execute();
        $result = $st->get_result();
        $conn->close();
        $investments = [];
        while($row = $result->fetch_assoc()) {
            array_push($investments, $row);
        }
        return $investments;
    }

    static function get_project_by_id($projectId) {
        $conn = self::get_connection();
        $sql = "select idProject as id, projectName as name, projectDescription as description, projectStartDate as startDate, 
       projectEndDate as endDate, requestedFund, idUser as userId from projects where idProject=?";
        $st = $conn->prepare($sql);
        $st->bind_param("i", $projectId);
        $st->execute();
        $result = $st->get_result();
        $conn->close();
        if($result->num_rows==0) {
            return null;
        }
        $project = null;
        while($row = $result->fetch_assoc()) {
            $project = $row;
            break;
        }
        return $project;
    }


    static function get_total_invested_sum_by_project_id($projectId) {
        $conn = self::get_connection();
        $sql = "select sum(pi.investmentFund) as totalInvestmentSum from projects p join projects_investors pi on p.idProject = pi.idProject where p.idProject=?";
        $st = $conn->prepare($sql);
        $st->bind_param("i", $projectId);
        $st->execute();
        $result = $st->get_result();
        $conn->close();
        while($row = $result->fetch_assoc()) {
            return $row['totalInvestmentSum'];
        }
        return null;
    }
    static function investment_exists($userId, $projectId) {
        $conn = self::get_connection();
        $sql = "select count(*) as c from projects_investors where idUser=? and idProject=?";
        $st = $conn->prepare($sql);
        $st->bind_param("ii", $userId, $projectId);
        $st->execute();
        $result = $st->get_result();
        $conn->close();
        while ($row = $result->fetch_assoc()) {
            if($row['c']>0) {
                return true;
            } else {
                false;
            }
        }
        return false;
    }

    static function create_investment($userId, $projectId, $fund, $date) {
        $conn = self::get_connection();
        $sql = "insert into projects_investors (idUser, idProject, investmentFund, investmentDate) values(?,?,?,?)";
        $st = $conn->prepare($sql);
        $st->bind_param("iiis", $userId, $projectId, $fund, $date);
        $st->execute();
        $result = $st->get_result();
        $conn->close();
    }

    static function get_connection() {
        $conn = new mysqli(self::DBSERVERNAME, self::DBUSERNAME, self::DBPASSWORD, self::DBNAME);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        return new mysqli(self::DBSERVERNAME, self::DBUSERNAME, self::DBPASSWORD, self::DBNAME);
    }
}
