<?php
session_start();
require_once 'dao.php';

if(!$_SESSION['user']) {
    header("Location: login.php");
    die();
}

if($_SERVER['REQUEST_METHOD']==='GET') {
    echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Create Investment</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-light bg-light">
    <a class="navbar-brand" style="cursor: pointer">Crowdfunding</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
  
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/hwp/logout.php">Logout</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-sm-4">';
    $project = Dao::get_project_by_id($_GET['projectId']);
    if($project==null) {
        header("Location: error.php?message=There is no such product");
        die();
    } else if ($project['userId']==$_SESSION['user']['id']) {
        header("Location: error.php?message=You cannot invest to your project");
        die();
    }  else if(Dao::investment_exists($_SESSION['user']['id'], $project['id'])) {
        header("Location: error.php?message=You have already invested to this project");
        die();
    }
    $totalInvestedSum = Dao::get_total_invested_sum_by_project_id($project['id']);
    $remaining = $project['requestedFund'] - $totalInvestedSum;
    echo "
            <p>Project name : $project[name]</p>
            <p>Remaining amount : $remaining</p>
            <p>Finishes at : $project[endDate]</p>";

    echo '
        </div>
        <div class="col-sm-6">
            <form action="/hwp/createInvestment.php" method="post">';
    echo "<input readonly hidden name='projectId' value='$project[id]'>";
    echo '
                <div class="form-group">
                    <label for="fund">Fund:</label>';
    echo "<input name='fund' type='number' step='0.01' min='0.01' max='$remaining' class='form-control' placeholder='Enter fund' id='fund' required>";
    echo '
                </div>
                <div class="form-group">
                    <label for="date">Due date:</label>';

    $today = date("Y-m-d");
    echo "
                    <input name='date' type='date' min='$today' max='$project[endDate]' class='form-control' placeholder='Enter date' id='date' required>";
    echo '            </div>
                <button type="submit" class="btn btn-primary">Invest</button>
            </form>
        </div>
        <div class="col-sm-2"></div>
    </div>

</div>
</body>';
} else {
    if(!isset($_POST['projectId']) || !isset($_POST['fund']) || !isset($_POST['date'])) {
        header("Location: error.php?message=Project id or fund of date params are missed");
        die();
    }
    $fund = $_POST['fund'];
    $date = $_POST['date'];
    $project = Dao::get_project_by_id($_POST['projectId']);
    $totalInvestedSum = Dao::get_total_invested_sum_by_project_id($project['id']);
    $remaining = $project['requestedFund'] - $totalInvestedSum;
    if($project['userId']==$_SESSION['user']['id']) {
        header("Location: error.php?message=You cannot invest to your project");
        die();
    } else if(Dao::investment_exists($_SESSION['user']['id'], $project['id'])) {
        header("Location: error.php?message=You have already invested to this project");
        die();
    } else if($fund>$remaining) {
        header("Location: error.php?message=Fund cannot be more than requested remaining sum");
        die();
    } else if($date>$project['endDate']) {
        header("Location: error.php?message=Date cannot be late than project's end date");
        die();
    }

    Dao::create_investment($_SESSION['user']['id'], $project['id'], $fund, $date);
    header("Location: home.php");
    die();
}
