<?php
session_start();
if(!$_SESSION['user']) {
    header("Location: login.php");
    die();
}
session_destroy();
header("Location: login.php");
die();